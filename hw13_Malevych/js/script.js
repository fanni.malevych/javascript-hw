const buttonChangeTheme = document.querySelector('.inverse-style')
const themeCssSelector = buttonChangeTheme.dataset.target;
const themeCssLink = document.querySelector(themeCssSelector);

buttonChangeTheme, addEventListener('click', function (e) {
    e.preventDefault()
    if (themeCssLink.href.includes("css/normal-theme.css")) {
        const newHref = themeCssLink.href.replace("css/normal-theme.css", "css/green-theme.css");
        themeCssLink.href = newHref;
        localStorage.setItem('theme', 'green')
    } else {
        const newHref = themeCssLink.href.replace("css/green-theme.css", "css/normal-theme.css");
        themeCssLink.href = newHref;
        localStorage.setItem('theme', 'normal')
    }
})
if (localStorage.getItem('theme') === 'green') {
    themeCssLink.setAttribute('href', 'css/green-theme.css')
}
else {
    themeCssLink.setAttribute('href', 'css/normal-theme.css')
}
